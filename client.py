import collections
import copy
import zmq
import signal
import sys
import threading
import time
from random import randint
import os
import message

HISTORY_SIZE = 10
SERVER_TIMEOUT = 3000
POLL_TIME = 100
INTERVAL_INIT = 1000 # Interval between destroying and creating a socket during reconnect
INTERVAL_MAX = 16000 # Max interval value

CONN_LIVENESS_INIT = SERVER_TIMEOUT / POLL_TIME 
CONN_LIVENESS_COEFF = 0.2 # Liveness = init * (1 +- coeff)

def cls():
    os.system('cls' if os.name=='nt' else 'clear')
    
def tprint(*a, **b):
    """Thread-safe print"""
    with lock_print:
        print(*a, **b)

class Listener(threading.Thread):
    def __init__(self, msgs, name, reconnect, ctx):
        threading.Thread.__init__ (self)
        self.ctx = ctx
        self.history = collections.deque(maxlen=HISTORY_SIZE)
        self.interval = INTERVAL_INIT
        self.kill_now = False
        self.msgs = msgs
        self.name = name
        self.reconnect_interval = reconnect
        
        self.socket_sub = None
        self.poll = None
    
    def create_socket(self):
        self.socket_sub = self.ctx.socket(zmq.SUB)
        self.socket_sub.setsockopt(zmq.SUBSCRIBE, b'')
        self.socket_sub.connect('tcp://localhost:5589')
        
        self.poll = zmq.Poller()
        self.poll.register(self.socket_sub, zmq.POLLIN)
        
        join_msg = message.Message(cls='JOIN', sender_id = self.name, sender_verb = self.name)
        join_msg_packed = join_msg.get_packed()
        with lock_msgs:
            self.msgs.append(join_msg_packed)
    
    def destroy_socket(self):
        self.poll.unregister(self.socket_sub)
        self.socket_sub.disconnect('tcp://localhost:5589')
        self.socket_sub.close()
        
    def reconnect(self):
        with lock_status_s:
            self.reconnect_interval = self.interval
        self.destroy_socket()
        time.sleep(self.interval//1000)
        self.interval = min(2*self.interval, INTERVAL_MAX)
        self.history.append('* Connecting to the server *')                    
        self.refresh()
        self.create_socket()
        
    def refresh(self):
        cls()
        for line in self.history:
            tprint(line)
        
    def run(self):
        self.history.clear()
        self.create_socket()
        
        conn_liveness = CONN_LIVENESS_INIT
        conn_flag = False
        ping_flag = False
        msg = message.Message()
        
        while True:
            poll_res = self.poll.poll(POLL_TIME)
            # if any incoming msg was received
            if zmq.POLLIN in [res[1] for res in poll_res]:
                for socket, event in poll_res:               
                    if event & zmq.POLLIN:
                        conn_liveness = randint(int(CONN_LIVENESS_INIT * (1-CONN_LIVENESS_COEFF)),int(CONN_LIVENESS_INIT * (1+CONN_LIVENESS_COEFF)))
                        if self.interval != INTERVAL_INIT:
                            self.interval = INTERVAL_INIT
                        msg_recv = socket.recv(zmq.NOBLOCK) 
                        msg.unpack_from(msg_recv)
                        msg_class = msg.get_class()
                        ping_flag = False
                        conn_flag = True
                        if (msg_class == "CHECK") and (self.name in msg.data):
                            if not self.msgs: # If no msgs to be send - then send IM ALIVE response
                                resp = message.Message(cls='ALIVE', sender_id=self.name, sender_verb=self.name)
                                resp_packed = resp.get_packed()
                                with lock_msgs:
                                    self.msgs.append(resp_packed)
                        elif msg_class == "PONG":
                            self.history.append("PONG")
                            self.refresh()
                        elif msg_class == "DATA": # Add check for server up / down
                            if msg.data['type'] == 'chat':
                                text = msg.data['text']
                                if msg.sender_verb:
                                    text = msg.sender_verb + ": " + text
                                self.history.append(text)                    
                                self.refresh()
                        elif (msg_class == "NOTREGD") and (self.name == msg.data):
                            join_msg = message.Message(cls='JOIN', sender_id = self.name, sender_verb = self.name)
                            join_msg_packed = join_msg.get_packed()
                            with lock_msgs:
                                self.msgs.append(join_msg_packed)
                        elif msg_class == "SRVR_DOWN":
                            self.history.append("* Server down *")
                            self.refresh()
                            conn_flag = False
            else:
                conn_liveness -= 1
                if conn_liveness == 0:
                    conn_liveness = CONN_LIVENESS_INIT
                    ping = message.Message(cls='PING', sender_id=self.name, sender_verb=self.name)
                    ping_packed = ping.get_packed()
                    with lock_msgs:
                        self.msgs.appendleft(ping_packed)
                    self.history.append("PING_")
                    self.history.append("TEST: " + str(ping_flag))
                    self.refresh()
                    if ping_flag:
                        if conn_flag:
                            self.history.append("* Lost connection *")
                            self.refresh()
                        self.reconnect()
                        ping_flag = False
                        conn_flag = False
                    else:    
                        ping_flag = True
       
            with lock_kill_l:
                if self.kill_now:
                    break
                    
        self.destroy_socket()

class Sender(threading.Thread):
    def __init__(self, msgs, name, reconnect, ctx):
        threading.Thread.__init__ (self)
        self.ctx = ctx
        self.msgs = msgs
        self.name = name
        self.kill_now = False
        self.reconnect_interval = reconnect
    
    def create_socket(self):
        self.socket_push = self.ctx.socket(zmq.PUSH)
        self.socket_push.connect('tcp://localhost:5588')
        
        self.poll = zmq.Poller()
        self.poll.register(self.socket_push, zmq.POLLOUT) 
        
    def destroy_socket(self):
        self.poll.unregister(self.socket_push)
        self.socket_push.setsockopt(zmq.LINGER, 0)
        self.socket_push.disconnect('tcp://localhost:5588')
        self.socket_push.close()
    
    def reconnect(self, interval):
        self.destroy_socket()
        time.sleep(interval//1000)
        self.create_socket()
        
    def run(self):
        local_queue = collections.deque()
        self.create_socket()
        
        while True:
            with lock_msgs:
                if self.msgs:
                    local_queue += self.msgs
                    self.msgs.clear()
            if local_queue:
                for socket, event in self.poll.poll(POLL_TIME):
                    if event & zmq.POLLOUT:
                        to_send = local_queue.popleft()
                        socket.send(to_send, zmq.NOBLOCK)
                
            with lock_status_s:
                if self.reconnect_interval:
                    self.reconnect(self.reconnect_interval)
                    self.reconnect_interval = 0
                elif self.kill_now:
                    break
        
        self.destroy_socket()

def main():

    def signal_handler(sig, frame):
        leave_msg = message.Message(cls='LEAVE', sender_id = name, sender_verb = name)
        leave_msg_packed = leave_msg.get_packed()
        with lock_msgs:
            msgs_queue.appendleft(leave_msg_packed)
        with lock_kill_l:
            listener.kill_now = True
        listener.join()
        with lock_status_s:
            sender.kill_now = True
        sender.join()
        ctx.term()
        sys.exit(0)
    
    msgs_queue = collections.deque()
    do_reconnect = 0 # If not 0, reconnect with set waiting interval
    #TEMP!!!!!!
    name = "T"+str(randint(0,1000))
    listener = Listener(msgs_queue, name, do_reconnect)
    sender = Sender(msgs_queue, name, do_reconnect)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGHUP, signal_handler)
    listener.start() 
    sender.start()  
        
    while True:
        text = input()
        if text:
            chat_msg = {'type' : 'chat', 'text' : text}
            msg = message.Message(cls='DATA', data=chat_msg, sender_id=name, sender_verb=name)
            msg_packed = msg.get_packed()
            with lock_msgs:
                msgs_queue.append(msg_packed)
        else:
            listener.refresh()

lock_msgs = threading.Lock()
lock_print = threading.Lock()
lock_kill_l = threading.Lock()
lock_status_s = threading.Lock()      
if __name__ == '__main__':
    main()
