import zmq
import collections
import signal
import sys
import threading
import time
from random import randint, random
import message

POLL_TIME = 1000
CLIENT_TIMEOUT = 5000
SERVER_TIMEOUT = 3000

def tprint(*a, **b):
    """Thread-safe print"""
    with lock_print:
        print(*a, **b)

class ServerPublisher(threading.Thread):
    def __init__(self, msgs):
        threading.Thread.__init__ (self)
        self.kill_now = False
        self.local_queue = collections.deque()
        self.msgs = msgs
    
    def run(self):
        socket_pub = ctx.socket(zmq.PUB)
        socket_pub.setsockopt(zmq.LINGER, 0)
        socket_pub.bind('tcp://127.0.0.1:5589')
        poll = zmq.Poller()
        poll.register(socket_pub, zmq.POLLOUT)  
        
        while True:
            if self.local_queue:
                for socket, event in poll.poll(POLL_TIME):
                    if event & zmq.POLLOUT:
                        to_send = self.local_queue.popleft()
                        socket.send(to_send, zmq.NOBLOCK)
            else:
                with lock_msgs:
                    if self.msgs:
                        self.local_queue = self.msgs.copy()
                        self.msgs.clear()
               
            with lock_kill_p:
                if self.kill_now:
                    break
                    
        poll.unregister(socket_pub)
        socket_pub.unbind('tcp://127.0.0.1:5589')
        socket_pub.close()
        
class ServerListener(threading.Thread):
    def __init__(self, msgs):
        threading.Thread.__init__ (self)
        self.connected = {}
        self.notregd = {}
        self.kill_now = False
        self.msgs = msgs
    
    def check_connected_status(self):
        t = round(time.time()*1000)
        send_check = False
        for ident in self.connected:
            tprint("TEST: "+str(t - self.connected[ident][0]))
            if t - self.connected[ident][0] > CLIENT_TIMEOUT:
                tprint("Client "+ident+" > CLIENT_TIMEOUT")
                if self.connected[ident][1]:
                    if (t - self.connected[ident][0]) > (2*CLIENT_TIMEOUT):
                        self.connected[ident][0] = -1 # Mark to remove
                        self.queue_announcement(ident + " timed out.")
                else:
                    self.connected[ident][1] = True
                    if not send_check:
                        send_check = True
            else:
                self.connected[ident][1] = False
        
        if send_check:
            check_ids_list = [i for i in self.connected if self.connected[i][1] == True]
            msg = message.Message(cls="CHECK", data=check_ids_list)
            msg_packed = msg.get_packed()
            with lock_msgs:
                self.msgs.append(msg_packed)
        
        # Delete dead clients
        self.connected = {k:v for k, v in self.connected.items() if v[0] != -1}
    
    def queue_announcement(self, text):
        chat_msg = {'type' : 'chat', 'text' : "* " + text + " *"}
        msg = message.Message(cls="DATA", data=chat_msg)
        msg_packed = msg.get_packed()
        with lock_msgs:
            self.msgs.append(msg_packed)
        
    def update_last_seen(self, ident):
        tprint('Update: '+ident)
        t = round(time.time()*1000)
        
        if (ident in self.connected):
            self.connected[ident][0] = t
        else: # Register new client: last_seen, check called?
            self.connected[ident] = [t, False]
        tprint(self.connected[ident])

    def run(self):
        socket_pull = ctx.socket(zmq.PULL)
        socket_pull.bind('tcp://127.0.0.1:5588')
        poll = zmq.Poller()
        poll.register(socket_pull, zmq.POLLIN)   
        
        tprint('Server started')
        msg = message.Message()
        last_pong = 0
        
        while True:
            self.check_connected_status()
            for socket, event in poll.poll(POLL_TIME):    
                if event & zmq.POLLIN:
                    received = socket.recv(zmq.NOBLOCK)
                    msg.unpack_from(received)
                    ident = msg.sender_id
                    cls = msg.get_class()
                    tprint('Received <<%s>> from %s' % (cls, ident))
                    
                    if ident not in self.connected:                        
                        if cls == "JOIN":
                            self.update_last_seen(ident)
                            self.queue_announcement(ident + " has joined.") 
                            if ident in self.notregd:
                                self.notregd.pop(ident)
                        elif ident not in self.notregd:
                            m = message.Message(cls="NOTREGD",data=ident)
                            m_packed = m.get_packed()
                            self.notregd[ident] = round(time.time()*1000)
                            with lock_msgs:
                                self.msgs.append(m_packed)   
                        elif round(time.time()*1000) - self.notregd[ident] > CLIENT_TIMEOUT:
                            self.notregd.pop(ident)
                            
                    elif cls == "LEAVE":
                        self.connected.pop(ident)
                        self.queue_announcement(ident + " has left.")
                        
                    elif cls == "PING" and round(time.time()*1000) - last_pong > SERVER_TIMEOUT:
                        self.update_last_seen(ident)
                        m = message.Message(cls="PONG")
                        m_packed = m.get_packed()
                        with lock_msgs:
                            self.msgs.append(m_packed)
                        last_pong = round(time.time()*1000)
                            
                    elif cls == "ALIVE":
                        self.update_last_seen(ident)
                        
                    elif cls == "DATA":
                        self.update_last_seen(ident)
                        with lock_msgs:
                            self.msgs.append(received)
                    
            
            with lock_kill_l:        
                if self.kill_now:
                    break

        poll.unregister(socket_pull)
        socket_pull.unbind('tcp://127.0.0.1:5588')
        socket_pull.close()


def main():

    def signal_handler(sig, frame):
        with lock_kill_l:
            listener.kill_now = True
        listener.join()
        msg = message.Message(cls="SRVR_DOWN")
        msg_packed = msg.get_packed()

        with lock_msgs:
            msgs_queue.clear()
            publisher.local_queue.clear()
            publisher.local_queue.append(msg_packed)
        time.sleep(0.1)
        with lock_kill_p:
            publisher.kill_now = True
        publisher.join()
        ctx.term()
        try:
            sys.exit(0)  
        except SystemExit as e:
            if e.code != 0:
                raise
    
    msgs_queue = collections.deque()       
    listener = ServerListener(msgs_queue)
    publisher = ServerPublisher(msgs_queue)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGHUP, signal_handler)
    listener.start()
    publisher.start()
    
if __name__ == '__main__':
    lock_msgs = threading.Lock()
    lock_kill_l = threading.Lock()
    lock_kill_p = threading.Lock()
    lock_print = threading.Lock()
    ctx = zmq.Context()
    main()
