import msgpack
import msg_class_codes as mcc

# https://github.com/msgpack/msgpack-python#packingunpacking-of-custom-data-type

class Message(object):
    def __init__(self, cls=None, data=None, sender_id=None, sender_verb=None):
        """
        cls is a string with message class code from msg_class_codes.py or None
        """
        if cls:
            if hasattr(mcc, cls):
                self.cls = getattr(mcc, cls)
            else:
                raise ValueError('Wrong message class code!')

        self.data = data
        self.sender_id = sender_id
        self.sender_verb = sender_verb
        
    def __repr__(self):
        return self.__class__.__name__+str(self.__dict__)
        
    def __str__(self):
        s = ""
        for k,v in self.__dict__.items():
            s += str(k)+" "+str(v)+"\n"
        return s
    
    def get_class(self):
        '''
        returns message class as a string from msg_class_codes.py
        '''
        for i in mcc.__dict__.items():
            if i[1] == self.cls:
                return i[0]
        return None
    
    def get_packed(self):
        '''
        returns msgpack object containing packed class instance
        '''
        return msgpack.packb(self.__dict__)
        
    def unpack_from(self, packed):
        # Obsluga bledow!?
        unpacked = msgpack.unpackb(packed, raw=False, use_list = False)
        self.__dict__.update(unpacked)
    
    def update(self, **kwargs):
        if 'cls' in kwargs:
            cls = kwargs.pop('cls')
            if hasattr(mcc, cls):
                self_as_dict['cls'] = getattr(mcc, cls)
            else:
                raise ValueError('Wrong message class code!')
        self_as_dict = self.__dict__
        self_as_dict.update({k : kwargs[k] for k in kwargs if k in self_as_dict})
